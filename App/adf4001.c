#include "adf4001.h"
#include "bsp.h"

USR_INT32U r_cter_latch_ch1;
USR_INT32U n_cter_latch_ch1;
USR_INT32U func_latch_ch1;
USR_INT32U init_latch_ch1;

USR_INT32U r_cter_latch_ch2;
USR_INT32U n_cter_latch_ch2;
USR_INT32U func_latch_ch2;
USR_INT32U init_latch_ch2;

void init_4001(USR_INT08U lchannal)
{
	switch(lchannal){
		case 1:
			r_cter_latch_ch1 = 0;
			n_cter_latch_ch1 = 1;
			func_latch_ch1 = 2;
			init_latch_ch1 = 3;
			break;
		case 2:
			r_cter_latch_ch2 = 0;
			n_cter_latch_ch2 = 1;
			func_latch_ch2 = 2;
			init_latch_ch2 = 3;
			break;
	}
}
USR_INT08U write_4001(USR_INT08U lchannal, USR_INT32U data)
{
	switch(lchannal){
		case 1:

			break;
		case 2:

			break;
	}
	return 0;
}
USR_INT08U write_r_cter(USR_INT08U lchannal, USR_INT32U latch)
{
	switch(lchannal){
		case 1:
			r_cter_latch_ch1 = 0x000000 | (latch << 2);
			adf4001_write(r_cter_latch_ch1);
			break;
		case 2:
			r_cter_latch_ch2 = latch;
			break;
	}
	return 0;
}
USR_INT08U write_n_cter(USR_INT08U lchannal, USR_INT32U latch)
{
	switch(lchannal){
		case 1:
			n_cter_latch_ch1 = 0x000001 | (latch << 8);
			adf4001_write(n_cter_latch_ch1);
			break;
		case 2:
			n_cter_latch_ch2 = latch;
			break;
	}
	return 0;
}
USR_INT08U write_func(USR_INT08U lchannal, USR_INT32U latch)
{

	switch(lchannal){
		case 1:
			func_latch_ch1 = latch;
			adf4001_write(func_latch_ch1);
			break;
		case 2:
			func_latch_ch2 = latch;
			break;
	}
	return 0;
}
USR_INT08U write_init(USR_INT08U lchannal, USR_INT32U latch)
{
	switch(lchannal){
		case 1:
			init_latch_ch1 = latch;
			adf4001_write(init_latch_ch1);
			break;
		case 2:
			init_latch_ch2 = latch;
			break;
	}
	return 0;
}
USR_INT32U read_r_cter(USR_INT08U lchannal)
{
	switch(lchannal){
		case 1:
			return r_cter_latch_ch1;
			break;
		case 2:
			return r_cter_latch_ch2;
			break;
	}
	return 0;
}
USR_INT32U read_n_cter(USR_INT08U lchannal)
{
	switch(lchannal){
		case 1:
			return n_cter_latch_ch1;
			break;
		case 2:
			return n_cter_latch_ch2;
			break;
	}
	return 0;
}
USR_INT32U read_func(USR_INT08U lchannal)
{
	switch(lchannal){
		case 1:
			return func_latch_ch1;
			break;
		case 2:
			return func_latch_ch2;
			break;
	}
	return 0;
}
USR_INT32U read_init(USR_INT08U lchannal)
{
	switch(lchannal){
		case 1:
			return init_latch_ch1;
			break;
		case 2:
			return init_latch_ch2;
			break;
	}
	return 0;
}
