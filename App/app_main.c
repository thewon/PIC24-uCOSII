/*
*********************************************************************************************************
*                                               uC/OS-II
*                                         The Real-Time Kernel
*
*                             (c) Copyright 1998-2005, Micrium, Weston, FL
*                                          All Rights Reserved
*
*
*                                      Microchip Application Code
*
* File : APP.C
* By   : Eric Shufo
*********************************************************************************************************
*/

#include "includes.h"
#include "mtype.h"

/*
*********************************************************************************************************
*                                                CONSTANTS
*********************************************************************************************************
*/
#define MSG_QUEUE_SZ	7					// MSG queue storage size

/*
*********************************************************************************************************
*                                                VARIABLES
*********************************************************************************************************
*/

OS_STK	AppStartTaskStk[APP_TASK_START_STK_SIZE];
OS_STK	AppHmc830TaskStk[APP_TASK_HMC830_STK_SIZE];
OS_STK	AppConsolTaskStk[APP_TASK_CONSOL_STK_SIZE];

void *msg_q[MSG_QUEUE_SZ];
OS_EVENT *consol_q;
/*
*********************************************************************************************************
*                                            FUNCTION PROTOTYPES
*********************************************************************************************************
*/

static  void  AppStartTask(void *p_arg);
/*
*********************************************************************************************************
*                                                main()
*
* Description : This is the standard entry point for C code.  It is assumed that your code will call
*               main() once you have performed all necessary 68HC12 and C initialization.
* Arguments   : none
*********************************************************************************************************
*/

USR_INT16S  main (void)
{
    USR_INT08U  err;
 //   USR_INT32U tmp;

	// change clock source here
	BSP_IntDisAll();                         /* Disable all interrupts until we are ready to accept them */
//	BSP_Init();

	OSInit();		                         /* Initialize "uC/OS-II, The Real-Time Kernel"              */
//	OSTaskCreate(ENTRY_ADDR(ConslTask), USR_NULL, AppConsolTaskStk, APP_TASK_CONSOL_PRIO);

	err = OSTaskCreateExt(	ENTRY_ADDR(AppStartTask),
							(void *)0,
							(OS_STK *)&AppStartTaskStk[0],
							APP_TASK_START_PRIO,
							APP_TASK_START_PRIO,
							(OS_STK *)&AppStartTaskStk[APP_TASK_START_STK_SIZE-1],
							APP_TASK_START_STK_SIZE,
							(void *)0,
							OS_TASK_OPT_STK_CHK | OS_TASK_OPT_STK_CLR);

	OSStart();                                                          /* Start multitasking (i.e. give control to uC/OS-II)       */

	return (-1);                                                        /* Return an error - This line of code is unreachable       */
}

/*$PAGE*/
/*
*********************************************************************************************************
*                                          STARTUP TASK
*
* Description : This is an example of a startup task.  As mentioned in the book's text, you MUST
*               initialize the ticker only once multitasking has started.
* Arguments   : p_arg   is the argument passed to 'AppStartTask()' by 'OSTaskCreate()'.
* Notes       : 1) The first line of code is used to prevent a compiler warning because 'p_arg' is not
*                  used.  The compiler should not generate any code for this statement.
*               2) Interrupts are enabled once the task start because the I-bit of the CCR register was
*                  set to 0 by 'OSTaskCreate()'.
*********************************************************************************************************
*/

static  void  AppStartTask (void *p_arg)
{
	USR_INT08U i;
	USR_INT08U j;
//	USR_INT32U addr;

   (void)p_arg;

    BSP_Init();                                                         /* Initialize BSP functions                                 */

	consol_q = OSQCreate(&msg_q[0], MSG_QUEUE_SZ);
	OSTaskCreate(ENTRY_ADDR(AppHmc830Task), USR_NULL, AppHmc830TaskStk, APP_TASK_HMC830_PRIO);
	OSTaskCreate(ENTRY_ADDR(ConslTask), USR_NULL, AppConsolTaskStk, APP_TASK_CONSOL_PRIO);
	
	while (USR_TRUE) {                                                      /* Task body, always written as an infinite loop.           */
		TEST_LED2 = 0;
		OSTimeDlyHMSM(0, 0, 1, 0);
		TEST_LED2 = 1;
		OSTimeDlyHMSM(0, 0, 1, 0);
	}
}
