/*********************************************************************
* FileName    : channal_01.c
* Dependencies: channal_01 API
* Processor   : PIC24F
* Complier    : Microchip C30 ,MPLAB IDE v8.90 or higher
* Company     : .
*
* Software License Agreement
*
*
*
* Author : Lau
* Date   : 7/14/2014
********************************************************************/
#include "mtype.h"
#include "hmc830lp6ge_type.h"

/*
*********************************************************************************************************
*                                         channel 01 PLL Register Map
*********************************************************************************************************
*/
REG00_ID			ch01_reg00;
REG01_RST			ch01_reg01;
REG02_REFDIV		ch01_reg02;
REG03_FRTNTG		ch01_reg03;
REG04_FRFRAC		ch01_reg04;
REG05_VCO			ch01_reg05;
REG06_SD			ch01_reg06;
REG07_LD			ch01_reg07;
REG08_ANLG			ch01_reg08;
REG09_CP			ch01_reg09;
REG0A_VCO_AUTOCAL	ch01_reg0A;
REG0B_PD			ch01_reg0B;
REG0C_F_F_C			ch01_reg0C;
REG0F_GPO_SPI_RDIV	ch01_reg0F;
REG10_VCO_TUNE		ch01_reg10;
REG11_SAR			ch01_reg11;
REG12_GPO2			ch01_reg12;
REG13_BIST			ch01_reg13;

/*
*********************************************************************************************************
*                                         channel 01 VCO Subsystem Register Map
* Note: the VCO subsystem uses indirect addressing via Reg 05h.
*********************************************************************************************************
*/

VCO_REG00_TUN			ch01_vco_reg00;
VCO_REG01_EN			ch01_vco_reg01;
VCO_REG02_BIAS			ch01_vco_reg02;
VCO_REG03_CFG			ch01_vco_reg03;
VCO_REG04_CAL_BIAS		ch01_vco_reg04;
VCO_REG05_CF_CAL		ch01_vco_reg05;
VCO_REG06_MSB_CAL		ch01_vco_reg06;

/*********************************************************************
 * Name        : void init_ch1()
 * Function    : channal 1 initial registers(memory)
 * PreCondition: None
 * Input       : @lchannal hmc830 channel number
 * Output      : NULL
 * Side Effects: None
 * Author      : Lau
 * Time/Creat  : 15:50 7/8/2014
 * Time/Modify : 09:53 7/15/2014
 * Note        : None
 ********************************************************************/
void init_ch1()
{
// init pll register 
	ch01_reg00.chip_id			= 0x00;
	ch01_reg01.chip_rst			= 0x02;
	ch01_reg02.chip_rdiv		= 0x01;
	ch01_reg03.chip_intg		= 0x19;
	ch01_reg04.chip_frac		= 0x00;
	ch01_reg05.chip_vco			= 0x00;
	ch01_reg06.chip_sd			= 0x200B4A;
	ch01_reg07.chip_ld			= 0x014D;
	ch01_reg08.chip_anlog		= 0xC1BEFF;
	ch01_reg09.chip_cp			= 0x403264;
	ch01_reg0A.chip_vco_autocal	= 0x2205;
	ch01_reg0B.chip_pd			= 0x0F8061;
	ch01_reg0C.chip_frac		= 0x00;
	ch01_reg0F.chip_gpo			= 0x01;
	ch01_reg10.chip_vco_tune	= 0x20;
	ch01_reg11.chip_sar			= 0x7FFF;
	ch01_reg12.chip_gpo2		= 0x00;
	ch01_reg13.chip_bist		= 0x00;
	
// init vco subsystem register
	ch01_vco_reg00.chip_tuning	= 0x00;
	ch01_vco_reg01.chip_enable	= 0x1F;
	ch01_vco_reg02.chip_biases	= 0x0C1;
	ch01_vco_reg03.chip_config	= 0x51;
	ch01_vco_reg04.chip_cal_bi	= 0x0C1;	//0x60A0;
	ch01_vco_reg05.chip_cf_cal	= 0x2C;		//0x1628;
	ch01_vco_reg06.chip_ms_cal	= 0x0FF;	//0x7FB0;

}
/*********************************************************************
 * Name        : USR_INT32U get_reg_ch1(OM_CMD_STRUCT *dst)
 * Function    : return channal 1 registers(memory)
 * PreCondition: None
 * Input       : @lchannal hmc830 channel number
 * Output      : register value
 * Side Effects: None
 * Author      : Lau
 * Time/Creat  : 15:50 7/8/2014
 * Time/Modify : 09:53 7/15/2014
 * Note        : None
 ********************************************************************/
USR_INT32U get_reg_ch1(OM_CMD_STRUCT *dst)
{
	switch(dst->dat_b.regibits){
	case REG00H:
		return (USR_INT32U)ch01_reg00.chip_id;
		break;
	case REG01H:
		return (USR_INT32U)ch01_reg01.chip_rst;
		break;
	case REG02H:
		return (USR_INT32U)ch01_reg02.chip_rdiv;
		break;
	case REG03H:
		return (USR_INT32U)ch01_reg03.chip_intg;
		break;
	case REG04H:
		return (USR_INT32U)ch01_reg04.chip_frac;
		break;
	case REG05H:						// Get reg 05H has no sense
		switch(dst->dat_v.regi_vco){
		case VCO_REG00H:
			return (USR_INT32U)ch01_vco_reg00.chip_tuning;
			break;
		case VCO_REG01H:
			return (USR_INT32U)ch01_vco_reg01.chip_enable;
			break;
		case VCO_REG02H:
			return (USR_INT32U)ch01_vco_reg02.chip_biases;
			break;
		case VCO_REG03H:
			return (USR_INT32U)ch01_vco_reg03.chip_config;
			break;
		case VCO_REG04H:
			return (USR_INT32U)ch01_vco_reg04.chip_cal_bi;
			break;
		case VCO_REG05H:
			return (USR_INT32U)ch01_vco_reg05.chip_cf_cal;
			break;
		case VCO_REG06H:
			return (USR_INT32U)ch01_vco_reg06.chip_ms_cal;
			break;
		}
		break;
	case REG06H:
		return (USR_INT32U)ch01_reg06.chip_sd;
		break;
	case REG07H:
		return (USR_INT32U)ch01_reg07.chip_ld;
		break;
	case REG08H:
		return (USR_INT32U)ch01_reg08.chip_anlog;
		break;
	case REG09H:
		return (USR_INT32U)ch01_reg09.chip_cp;
		break;
	case REG0AH:
		return (USR_INT32U)ch01_reg0A.chip_vco_autocal;
		break;
	case REG0BH:
		return (USR_INT32U)ch01_reg0B.chip_pd;
		break;
	case REG0CH:
		return (USR_INT32U)ch01_reg0C.chip_frac;
		break;
	case REG0FH:
		return (USR_INT32U)ch01_reg0F.chip_gpo;
		break;
	case REG10H:
		return (USR_INT32U)ch01_reg10.chip_vco_tune;
		break;
	case REG11H:
		return (USR_INT32U)ch01_reg11.chip_sar;
		break;
	case REG12H:
		return (USR_INT32U)ch01_reg12.chip_gpo2;
		break;
	case REG13H:
		return (USR_INT32U)ch01_reg13.chip_bist;
		break;
	default :
		return (USR_INT32U)ch01_reg00.chip_id;
		break;
	}
	return -1;
}

/*********************************************************************
 * Name        : void set_pll_reg(OM_CMD_STRUCT *src)
 * Function    : modify channal 1 registers(memory)
 * PreCondition: None
 * Input       : @lchannal hmc830 channel number
 * Output      : NULL
 * Side Effects: None
 * Author      : Lau
 * Time/Creat  : 15:50 7/8/2014
 * Time/Modify : 10:03 7/15/2014
 * Note        : None
 ********************************************************************/
void set_reg_ch1(OM_CMD_STRUCT *src)
{
//	OM_CMD_STRUCT src;
	switch(src->dat_b.regibits){
	case REG00H:
		ch01_reg00.chip_id = src->dat_b.databits;
		break;
	case REG01H:
		ch01_reg01.chip_rst = src->dat_b.databits;
		break;
	case REG02H:
		ch01_reg02.chip_rdiv = src->dat_b.databits;
		break;
	case REG03H:
		ch01_reg03.chip_intg = src->dat_b.databits;
		break;
	case REG04H:
		ch01_reg04.chip_frac = src->dat_b.databits;
		break;
	case REG05H:
		switch(src->dat_v.regi_vco){
		case VCO_REG00H:
			ch01_vco_reg00.chip_tuning = src->dat_v.data_vco;
			break;
		case VCO_REG01H:
			ch01_vco_reg01.chip_enable = src->dat_v.data_vco;
			break;
		case VCO_REG02H:
			ch01_vco_reg02.chip_biases = src->dat_v.data_vco;
			break;
		case VCO_REG03H:
			ch01_vco_reg03.chip_config = src->dat_v.data_vco;
			break;
		case VCO_REG04H:
			ch01_vco_reg04.chip_cal_bi = src->dat_v.data_vco;
			break;
		case VCO_REG05H:
			ch01_vco_reg05.chip_cf_cal = src->dat_v.data_vco;
			break;
		case VCO_REG06H:
			ch01_vco_reg06.chip_ms_cal = src->dat_v.data_vco;
			break;
		}
		break;
	case REG06H:
		ch01_reg06.chip_sd = src->dat_b.databits;
		break;
	case REG07H:
		ch01_reg07.chip_ld = src->dat_b.databits;
		break;
	case REG08H:
		ch01_reg08.chip_anlog = src->dat_b.databits;
		break;
	case REG09H:
		ch01_reg09.chip_cp = src->dat_b.databits;
		break;
	case REG0AH:
		ch01_reg0A.chip_vco_autocal = src->dat_b.databits;
		break;
	case REG0BH:
		ch01_reg0B.chip_pd = src->dat_b.databits;
		break;
	case REG0CH:
		ch01_reg0C.chip_frac = src->dat_b.databits;
		break;
	case REG0FH:
		ch01_reg0F.chip_gpo = src->dat_b.databits;
		break;
	case REG10H:
		ch01_reg10.chip_vco_tune = src->dat_b.databits;
		break;
	case REG11H:
		ch01_reg11.chip_sar = src->dat_b.databits;
		break;
	case REG12H:
		ch01_reg12.chip_gpo2 = src->dat_b.databits;
		break;
	case REG13H:
		ch01_reg13.chip_bist = src->dat_b.databits;
		break;
	default :
		break;
	}
}
