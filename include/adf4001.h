#ifndef __ADF4001_H
#define __ADF4001_H

#include "mtype.h"
// data latch           control bits
#define RCTR_LATCH		00
#define NCTR_LATCH		01
#define FUNC_LATCH		10
#define INIT_LATCH		11

// REFERENCE COUNTER LATCH
//enum REF_LATCH{LDP,TM_BITS,ABP_WTH,REF_CTER,CBITS};
// N COUNTER LATCH
//enum N_CTER_LATCH{CP_GAIN, N_CTER, CBITS};
// FUNCTION LATCH
//enum FUNCTION_LATCH{PD2, CRR_SET2, CRR_SET1, TCC, FLM, FLE, CP_THREE, PH_DET, MUX_CTRL, PD1, CTER_RES, CBITS};
// INITIALIZATION LATCH
//enum INITILIZATION_LATCH{PD2, CRR_SET2, CRR_SET1, TCC, FLM, FLE, CP_THREE, PH_DET, MUX_CTRL, PD1, CTER_RES, CBITS};

void init_4001(USR_INT08U lchannal);
USR_INT08U write_4001(USR_INT08U lchannal, USR_INT32U data);
USR_INT08U write_r_cter(USR_INT08U lchannal, USR_INT32U latch);
USR_INT08U write_n_cter(USR_INT08U lchannal, USR_INT32U latch);
USR_INT08U write_func(USR_INT08U lchannal, USR_INT32U latch);
USR_INT08U write_init(USR_INT08U lchannal, USR_INT32U latch);
USR_INT32U read_r_cter(USR_INT08U lchannal);
USR_INT32U read_n_cter(USR_INT08U lchannal);
USR_INT32U read_func(USR_INT08U lchannal);
USR_INT32U read_init(USR_INT08U lchannal);

#endif
