/*
*********************************************************************************************************
*                                               Microchip PIC33
*                                            Board Support Package
*
*																								   Micrium
*                                    (c) Copyright 2005, Micrium, Weston, FL
*                                              All Rights Reserved
*
*
* File : BSP.H
* By   : Eric Shufro
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*                                      OSCILLATOR FREQUENCIES
*********************************************************************************************************
*/
#ifndef __BSP_H
#define __BSP_H

#include "mtype.h"

#define	CPU_PRIMARY_OSC_FRQ			8000000L                               /* Primary External Oscillator Frequency                */
#define	CPU_FRC_OSC_FRQ				8000000L                               /* Internal Fast Oscillator Frequency                   */
#define	CPU_SECONDARY_OSC_FRQ		32768L                               /* Secondary External Oscillator Frequency              */
#define	CPU_LPRC_OSC_FRQ			31000L                               /* Low Power Oscillator (LPOSC) 31Khz Nominal           */

/*
*********************************************************************************************************
*                                      OS TICK TIMER SELECTION
*********************************************************************************************************
*/

#define  BSP_OS_TMR_SEL                     2                               /* Select a timer for the OS Tick Interrupt (2 or 4)    */
#define  TIMER_INT_PRIO                     4                               /* Configure the timer to use interrupt priority 4      */
#define  OSVIEW_INT_PRIO                    4                               /* Configure UART2 Interrupts to use priority 4         */

/*
*********************************************************************************************************
*                                             DATATYPES
*********************************************************************************************************
*/

typedef  void (*PFNCT)(void);

/*
*********************************************************************************************************
*                                             MACROS
*********************************************************************************************************
*/

#define UNLOCK_REG() do{asm volatile(	"MOV	#OSCCON, w1 \n" \
										"MOV	#0x46, w2 \n" \
										"MOV	#0x57, w3 \n" \
										"MOV.b	w2, [w1] \n" \
										"MOV.b	w3, [w1] \n" \
										"BCLR	OSCCON, #6 \n" \
									);	\
					}while(0)
#define LOCK_REG()	do{asm volatile(	"MOV	#OSCCON, w1 \n" \
										"MOV	#0x46, w2 \n" \
										"MOV	#0x57, w3 \n" \
										"MOV.b	w2, [w1] \n" \
										"MOV.b	w3, [w1] \n" \
										"BSET	OSCCON, #6" \
									);	\
					}while(0)
					
#define CLOCK_SW()  do{asm volatile(	"MOV	#OSCCONH, w1 \n" \
										"MOV	#0x78, w2 \n" \
										"MOV	#0x9A, w3 \n" \
										"MOV.b	w2, [w1] \n" \
										"MOV.b	w3, [w1]" \n\
										\
										"MOV.b	#01, OSCCONH \n" \
										\
										"MOV	#OSCCONL, w1 \n" \
										"MOV	#0x46, w2 \n" \
										"MOV	#0x57, w3 \n" \
										"MOV.b	w2, [w1] \n" \
										"MOV.b	w3, [w1] \n" \
										\
										"BSET	OSCCON, #0" \
									); \
					}while(0)
					
/*
*********************************************************************************************************
*                                       CHIP SPECIFIC MACROS
*********************************************************************************************************
*/
                                                                        /* OSCCON Register Bits                                     */
#define  XT_HS_EC_PLL_SEL    (3 <<  0)
#define  COSC_MASK           (7 << 12)
#define  LOCK                (1 <<  5)
#define  OSWEN               (1 <<  0)
                                                                        /* CLKDIV Register Bits                                     */
#define  FRCDIV_MASK         (7 <<  8)
#define  PLLPOST_MASK        (3 <<  6)
#define  PLLPRE_MASK      (0x1F <<  0)
#define  PLLDIV_MASK      (0xFF <<  0)
                                                                        /* Timer Control Register Bits                              */
#define  TON                 (1 << 15)
                                                                        /* IPC1 Interrupt Priority Register Bits                    */
#define  T2IP_MASK           (7 << 12)  
                                                                        /* IPC5 Interrupt Priority Register Bits                    */
#define  T4IP_MASK           (7 << 12)  
                                                                        /* IPC7 Interrupt Priority Register Bits                    */
#define  U2TXIP_MASK_MASK         (7 << 12)  
#define  U2RXIP_MASK_MASK         (7 <<  8) 
                                                                        /* IEC0 Interrupt Enable Register Bits                      */
#define  T2IE                (1 <<  7)
                                                                        /* IEC1 Interrupt Enable Register Bits                      */
#define  T4IE                (1 << 11)
#define  U2TXIE_MASK              (1 << 15)
#define  U2RXIE_MASK              (1 << 14)
                                                                        /* IFS0 Interrupt Flag Register Bits                        */
#define  T2IF                (1 <<  7)
                                                                        /* IFS1 Interrupt Flag Register Bits                        */
#define  T4IF                (1 << 11)
#define  U2TXIF_MASK              (1 << 15)
#define  U2RXIF_MASK              (1 << 14)
                                                                        /* UxMODE Register                                          */
#define  UART_EN_MASK             (1 << 15)
                                                                        /* UxSTA Register                                           */
#define  UTXISEL_MASK             (1 << 15)
#define  UTXEN_MASK               (1 << 10)
#define  TRMT_MASK                (1 <<  8)
#define  URXDA_MASK               (1 <<  0)
                                                            
/*
*********************************************************************************************************
*                                       FUNCTION PROTOTYPES
*********************************************************************************************************
*/

void     BSP_Init(void);
void     BSP_IntEn (USR_INT08U IntCont, USR_INT08U IntNum, USR_INT08U IntPol, USR_INT08U IntAct, PFNCT pfnct);
void     BSP_IntDis (USR_INT08U IntCont, USR_INT08U IntNum);
void     BSP_IntDisAll(void);

USR_INT32U   BSP_CPU_ClkFrq(void);

/*
*********************************************************************************************************
*                                          TICK SERVICES
*********************************************************************************************************
*/

void     Tmr_TickISR_Handler(void);

/*
*********************************************************************************************************
*                                      		  SPI1
*********************************************************************************************************
*/
#define SPI1_SCK		PORTBbits.PORTB4	// rp28
#define SPI1_SDO		PORTBbits.PORTB8	// rp8
#define SPI1_SDI		PORTBbits.PORTB9	// rp9
#define SPI1_CH1_SEN	PORTAbits.PORTA9
//#define TEST_LED1		PORTBbits.PORTB10
#define TEST_LED2		PORTAbits.PORTA10

USR_INT08U * SPI_Send(USR_INT08U lchannal, USR_INT08U *buf, USR_INT08U Length);
USR_INT08U SPI_Receive( USR_INT08U *buf);


/*
*********************************************************************************************************
*                                      		  HMC830 serial port mode
*********************************************************************************************************
*/
typedef enum{OPENMODE= 1, HMCMODE}SPIMODE;

/*
*********************************************************************************************************
*                                      		  ADF4001
*********************************************************************************************************
*/
//#define ADF4001_LE		PORTBbits.PORTB10
//#define ADF4001_CLK		PORTBbits.PORTB6
//#define ADF4001_DAT		PORTBbits.PORTB7

#define ADF4001_LE		LATBbits.LATB10
#define ADF4001_CLK		LATBbits.LATB6
#define ADF4001_DAT		LATBbits.LATB7
void adf4001_write(USR_INT32U dat);
/*
*********************************************************************************************************
*                                      		  UART
*********************************************************************************************************
*/

inline void echo_char(const USR_CHAR lch);
inline void echo_str(const USR_CHAR *lstr, USR_INT08U len);


#endif
