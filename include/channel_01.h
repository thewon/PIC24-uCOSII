/*********************************************************************
* FileName    : hmc830lp6ge.c
* Dependencies: hmc830 API
* Processor   : PIC24F
* Complier    : Microchip C30 ,MPLAB IDE v8.90 or higher
* Company     : .
*
* Software License Agreement
*
*
*
* Author : Lau
* Date   : 7/14/2014
********************************************************************/
#ifndef __CHANNEL_01_H
#define __CHANNEL_01_H

#include "hmc830lp6ge_type.h"
/*
*********************************************************************************************************
*                                 channel 01 functions
* Note: 
*********************************************************************************************************
*/

void init_ch1();
USR_INT32U get_reg_ch1(OM_CMD_STRUCT *dst);
void set_reg_ch1(OM_CMD_STRUCT *src);

#endif
