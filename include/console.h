/*********************************************************************
* FileName    : console.h
* Dependencies: 
* Processor   : PIC24F
* Complier    : Microchip C30 ,MPLAB IDE v8.90 or higher
* Company     : .
*
* Software License Agreement
*
*
*
* Author : Lau
* Date   : 7/8/2014
********************************************************************/

#ifndef __CONSOLE_H
#define __CONSOLE_H

#include "p24FJ256GB110.h"
#include "mtype.h"

/*
*****************************************************************************
* 								FIFO structure variation
* Note: extern declaration
*****************************************************************************
*/
typedef enum {ILLIGLE_CH = 0, OCTNUMB_CH, HEXNUMB_CH, WORD_CH}CHECKRULE;
/*
*****************************************************************************
* 								character check rules MACRO
* Note: 
*****************************************************************************
*/



void ConslTask(void *p_arg);

#endif
