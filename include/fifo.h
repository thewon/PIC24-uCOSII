/*********************************************************************
* FileName    : fifo.h
* Dependencies: 
* Processor   : PIC24F
* Complier    : Microchip C30 ,MPLAB IDE v8.90 or higher
* Company     : .
*
* Software License Agreement
*
*
*
* Author : Lau
* Date   : 7/8/2014
********************************************************************/

#ifndef __FIFO_H
#define __FIFO_H

#include "mtype.h"
#include "p24FJ256GB110.h"
/*
*****************************************************************************
* 								FIFO structure variation
* Note: extern declaration
*****************************************************************************
*/
extern USR_INT08U*	Fifo_S;
extern USR_INT08U*	Fifo_E;
extern USR_INT08U*	Fifo_R;
extern USR_INT08U*	Fifo_W;

/*
*****************************************************************************
* 								FIFO structure protect MACRO
* Note: disnable and enable interrupt
*****************************************************************************
*/

#define FIFO_USE_INTE_MAC

#ifdef FIFO_USE_INTE_MAC
#define	 U1RXI_EN()		(IEC0bits.U1RXIE = 1)		// enable uart1 RX interrupt
#define  U1RXI_DIS()	(IEC0bits.U1RXIE = 0)		// disable uart1 RX interrupt
#else
#define	 U1RXI_EN()
#define  U1RXI_DIS()
#endif

#define MIN(a, b) (((a) > (b)) ? (b) : (a))

#define INTE_WR_FIFO() \
		do{ \
			*Fifo_W = U1RXREG; \
			if (Fifo_W < Fifo_E) Fifo_W ++; \
			else Fifo_W = Fifo_S; \
			if (Fifo_W == Fifo_R) Fifo_R ++; \
		}while(0)

/*
*****************************************************************************
* 								FIFO function
* Note: FIFO function
*****************************************************************************
*/
void FIFO_Init(void);
USR_INT16S FIFO_Read(USR_INT08U* pBuffer,USR_INT16S len);
USR_INT16S FIFO_Write(USR_INT08U* pBuffer,USR_INT16S len);
USR_INT16S FIFO_BlankNum(void);
USR_INT16S FIFO_HoldNum(void);

#endif
