/*********************************************************************
* FileName    : hmc830lp6ge.h
* Dependencies: hmc830 API
* Processor   : PIC24F
* Complier    : Microchip C30 ,MPLAB IDE v8.90 or higher
* Company     : .
*
* Software License Agreement
*
*
*
* Author : Lau
* Date   : 7/15/2014
********************************************************************/
#ifndef __HMC830LP6GE_H
#define __HMC830LP6GE_H

#include "mtype.h"
/*
*********************************************************************************************************
*                                 hmc830 api functions
* Note: 
*********************************************************************************************************
*/
extern void init_channal(USR_INT08U lchannal);
extern USR_INT32U print_pll(USR_INT08U lchannal, USR_INT08U reg_addr);
USR_INT32U print_vco(USR_INT08U lchannal, USR_INT08U vco_addr);
void write_pll(USR_INT08U lchannal, USR_INT08U reg_addr, USR_INT32U reg_valu);
void write_vco(USR_INT08U lchannal, USR_INT08U vco_addr, USR_INT32U vco_valu);
USR_INT08U read_pll(USR_INT08U lchannal, USR_INT08U reg_addr);
void out_freq(USR_INT08U lchannal, USR_FP64 rf_freq);
void chage_gain(USR_INT08U lchannal, USR_INT08U gain);

#endif
