/*********************************************************************
* FileName    : hmc830lp6ge_type.h
* Dependencies: hmc830 type define and regiter structure define
* Processor   : PIC24F
* Complier    : Microchip C30 ,MPLAB IDE v8.90 or higher
* Company     : .
*
* Software License Agreement
*
*
*
* Author : Lau
* Date   : 7/14/2014
********************************************************************/
#ifndef __HMC830LP6GE_TYPE_H
#define __HMC830LP6GE_TYPE_H

#include "mtype.h"

/*
*********************************************************************************************************
*                                              PLL Register Map
*********************************************************************************************************
*/

typedef union _REG00H_ID{
	USR_INT32U	chip_id;							// Read Only
	struct{											// Write Only
		USR_INT32U read_addr:5;						// Bits region
		USR_INT32U soft_rest:1;
		USR_INT32U not_def:18;
		USR_INT32U padding:8;
	}bits;
}REG00_ID;

typedef union _REG01H_RST{							// R/W
	USR_INT32U	chip_rst;							// Whole region
	struct{											// Bits region
		USR_INT32U rst_sel:1;
		USR_INT32U rst_spi:1;
		USR_INT32U keep_bias_on:1;
		USR_INT32U keep_pd_on:1;
		USR_INT32U keep_cp_on:1;
		USR_INT32U keep_ref_buf_on:1;
		USR_INT32U keep_vco_on:1;
		USR_INT32U keep_gpo_driver_on:1;
		USR_INT32U reserved0:1;
		USR_INT32U reserved1:1;
		USR_INT32U reserved2:14;
		USR_INT32U padding:8;
	}bits;
}REG01_RST;

typedef union _REG02H_REFDIV{						// R/W
	USR_INT32U	chip_rdiv;							// Whole region
	struct{											// Bits region
		USR_INT32U rdiv:14;
		USR_INT32U reserved:10;
		USR_INT32U padding:8;
	}bits;
}REG02_REFDIV;

typedef union _REG03H_FRTNTG{						// R/W
	USR_INT32U	chip_intg;							// Whole region
	struct{											// Bits region
		USR_INT32U intg:19;
		USR_INT32U reserved:5;
		USR_INT32U padding:8;
	}bits;
}REG03_FRTNTG;

typedef union _REG04H_FRFRAC{						// R/W
	USR_INT32U	chip_frac;							// Whole region
	struct{											// Bits region
		USR_INT32U frac:24;
		USR_INT32U padding:8;
	}bits;
}REG04_FRFRAC;

typedef union _REG05H_VCO{							// R/W
	USR_INT32U	chip_vco;							// Whole region
	struct{											// Bits region
		USR_INT32U vco_id:3;
		USR_INT32U vco_addr:4;
		USR_INT32U vco_data:9;
		USR_INT32U reserved:16;
		USR_INT32U padding:8;
	}bits;
}REG05_VCO;

typedef union _REG06H_SD{							// R/W
	USR_INT32U	chip_sd;							// Whole region
	struct{											// Bits region
		USR_INT32U seed:2;
		USR_INT32U order:2;
		USR_INT32U reserved0:3;						// Program 100
		USR_INT32U frac_bypass:1;
		USR_INT32U autoseed:1;
		USR_INT32U clkrq_refdiv_sel:1;
		USR_INT32U sd_modul_clk_sel:1;				// Program 1
		USR_INT32U sd_enable:1;
		USR_INT32U reserved1:1;						// Program 0
		USR_INT32U spare:1;							// Don't care
		USR_INT32U reserved2:2;						// Program 0
		USR_INT32U reserved3:2;						// Program 11 for PFD rate > 50MHz. 0 for <= 50MHz
		USR_INT32U bist_en:1;						// Program 0
		USR_INT32U rdiv_bist_cyc:2;					// Program 00
		USR_INT32U auto_clock_cfg:1;				// Program 0
		USR_INT32U reserved4:1;						// Program 0
		USR_INT32U reserved5:1;
		USR_INT32U padding:8;
	}bits;
}REG06_SD;

typedef union _REG07H_LD{							// R/W
	USR_INT32U	chip_ld;							// Whole region
	struct{											// Bits region
		USR_INT32U ld_wincnt_max:3;
		USR_INT32U inter_ld_en:1;
		USR_INT32U reserved0:2;						// 
		USR_INT32U ld_win_type:1;
		USR_INT32U ld_digi_win_dura:3;
		USR_INT32U ld_digi_tm_freq_ctr:2;
		USR_INT32U ld_tm_test_mode:1;				// 
		USR_INT32U auto_relock:1;
		USR_INT32U reserved1:10;
		USR_INT32U padding:8;
	}bits;
}REG07_LD;

typedef union _REG08H_ANLG{							// R/W
	USR_INT32U	chip_anlog;							// Whole region
	struct{											// Bits region
		USR_INT32U bias_en:1;						// Program 1
		USR_INT32U cp_en:1;							// Program 1
		USR_INT32U pd_en:3;							// Program 1
		USR_INT32U refbuf_en:1;						// Program 1
		USR_INT32U vcobuf_en:1;						// Program 1
		USR_INT32U gpo_pad_en:1;
		USR_INT32U reserved0:1;						// Program 1
		USR_INT32U vco_div_clktodig:1;				// Program 1
		USR_INT32U reserved1:1;						// Program 0
		USR_INT32U prescaler_clk_en:1;				// Program 1
		USR_INT32U vcobuf_prebias_en:1;				// Program 1
		USR_INT32U cp_inte_opa_en:1;				// Program 1
		USR_INT32U reserved2:3;						// Program 011
		USR_INT32U reserved3:3;						// Program 011
		USR_INT32U spare0:1;						// Don't care
		USR_INT32U reserved4:1;						// Program 011
		USR_INT32U reserved5:1;						// Program 011
		USR_INT32U high_freg_ref:1;					// Program 1 for Ref In > =200 MHz, 0 <200MHz
		USR_INT32U spare1:1;						// Don't care
		USR_INT32U spare2:1;						// Don't care
		USR_INT32U padding:8;
	}bits;
}REG08_ANLG;

typedef union _REG09H_CP{							// R/W
	USR_INT32U	chip_cp;							// Whole region
	struct{											// Bits region
		USR_INT32U cp_dn_gain:7;
		USR_INT32U cp_up_gain:7;
		USR_INT32U offset_magnitude:7;				// 
		USR_INT32U offset_up_en:1;					// Recommended setting = 0
		USR_INT32U offset_dn_en:1;					// Recommended setting = 1 in Fractional Mode, 0 otherwise
		USR_INT32U hikcp:1;
		USR_INT32U padding:8;
	}bits;
}REG09_CP;

typedef union _REG0AH_VCO_AUTOCAL{					// R/W
	USR_INT32U	chip_vco_autocal;					// Whole region
	struct{											// Bits region
		USR_INT32U vturn_resolution:3;
		USR_INT32U vco_curve_adj:3;
		USR_INT32U wait_state_setup:2;				// 
		USR_INT32U num_sar_vco:2;					// 0: 8 - Use for Wideband devices
		USR_INT32U force_curve:1;					// Program 0
		USR_INT32U bypass_vco_tuning:1;
		USR_INT32U no_vspi_trigger:1;				// Program 0,If 1, serial transfers to VCO sub-system (via Reg 05)are disabled
		USR_INT32U fsm_vspi_clk_sel:2;
		USR_INT32U xtal_falling_edge_fsm:1;			// Program 0
		USR_INT32U force_rdivider_bypass:1;			// Program 0
		USR_INT32U reserved1:7;
		USR_INT32U padding:8;
	}bits;
}REG0A_VCO_AUTOCAL;

typedef union _REG0BH_PD{							// R/W
	USR_INT32U	chip_pd;							// Whole region
	struct{											// Bits region
		USR_INT32U pd_del_sel:3;					// Program 001 Sets PD reset path delay
		USR_INT32U pd_short_input:1;				// Program 0 for normal operation.
		USR_INT32U pd_phase_sel:1;					// Program 0 for typical operation.
		USR_INT32U pd_up_en:1;						// Program 1 Enables the PD UP output
		USR_INT32U pd_dn_en:1;						// Program 1 Enables the PD DN output
		USR_INT32U csp_mode:2;
		USR_INT32U force_cp_up:1;					// Program
		USR_INT32U force_cp_dn:1;					// Program
		USR_INT32U force_cp_mld:1;					// Program 
		USR_INT32U reserved0:3;						// Program 100 for HMC830
		USR_INT32U cp_inte_opa_bias:2;				// Program 11
		USR_INT32U mcnt_clk_gating:2;				// Program 11(All Clocks ON (Recommended))
		USR_INT32U spare0:1;						// Don't care
		USR_INT32U reserved1:2;						// Program 00
		USR_INT32U reserved2:2;						// Program 00
		USR_INT32U padding:8;
	}bits;
}REG0B_PD;

typedef union _REG0CH_FINE_FREQ_CORR{				// R/W
	USR_INT32U	chip_frac;							// Whole region
	struct{											// Bits region
		USR_INT32U num_chann_per_fpd:14;
		USR_INT32U reserved0:10;
		USR_INT32U padding:8;
	}bits;
}REG0C_F_F_C;

typedef union _REG0FH_GPO_SPI_RDIV{					// R/W
	USR_INT32U	chip_gpo;							// Whole region
	struct{											// Bits region
		USR_INT32U gpo_sel:5;
		USR_INT32U gpo_test_data:1;
		USR_INT32U automux_sdo:1;					// 
		USR_INT32U ldo_driv_aw_on:1;				// Recommended setting = 0
		USR_INT32U pfet_dis:1;						// Program 0
		USR_INT32U nfet_dis:1;						// Program 0
		USR_INT32U reserved0:14;
		USR_INT32U padding:8;
	}bits;
}REG0F_GPO_SPI_RDIV;

typedef union _REG10H_VCO_TUNE{						// Read Only
	USR_INT32U	chip_vco_tune;						// Whole region
	struct{											// Bits region
		USR_INT32U vco_sw_set:8;
		USR_INT32U autocal_busy:1;
		USR_INT32U reserved0:15;
		USR_INT32U padding:8;
	}bits;
}REG10_VCO_TUNE;

typedef union _REG11H_SAR{							// Read Only
	USR_INT32U	chip_sar;							// Whole region
	struct{											// Bits region
		USR_INT32U sar_err_mag_cnt:19;
		USR_INT32U sar_err_sign:1;
		USR_INT32U reserved0:4;
		USR_INT32U padding:8;
	}bits;
}REG11_SAR;

typedef union _REG12H_GPO2{							// Read Only
	USR_INT32U	chip_gpo2;							// Whole region
	struct{											// Bits region
		USR_INT32U gpo:19;
		USR_INT32U lock_detect:1;
		USR_INT32U reserved0:4;
		USR_INT32U padding:8;
	}bits;
}REG12_GPO2;

typedef union _REG13H_BIST{							// Read Only
	USR_INT32U	chip_bist;							// Whole region
	struct{											// Bits region
		USR_INT32U bist_sign:16;
		USR_INT32U bist_busy:1;
		USR_INT32U reserved0:7;
		USR_INT32U padding:8;
	}bits;
}REG13_BIST;

/*
*********************************************************************************************************
*                                         VCO Subsystem Register Map
* Note: the VCO subsystem uses indirect addressing via Reg 05h.
*********************************************************************************************************
*/

typedef union _VCO_REG00H_TUN{						// Write Only
	USR_INT16U	chip_tuning;						// Whole region
	struct{											// Bits region
		USR_INT16U cal:1;
		USR_INT16U caps:8;
		USR_INT16U padding:7;
	}bits;
}VCO_REG00_TUN;

typedef union _VCO_REG01H_EN{						// Write Only
	USR_INT16U	chip_enable;						// Whole region
	// Wideband parts
	struct{											// Bits region
		USR_INT16U mast_en_vco_subsys:1;
		USR_INT16U manu_mo_pllbuf_en:1;
		USR_INT16U manu_mo_rfbuf_en:1;
		USR_INT16U manu_mo_drv_1_en:1;
		USR_INT16U manu_mo_rfdrv_en:1;
		USR_INT16U spare:4;							// Don’t care
		USR_INT16U padding:7;
	}bits;
}VCO_REG01_EN;

typedef union _VCO_REG02H_BIAS{						// Write Only
	USR_INT16U	chip_biases;						// Whole region
	// Wideband parts
	struct{											// Bits region
		USR_INT16U rf_drv_ratio:6;
		USR_INT16U rf_obuf_gain:2;
		USR_INT16U div_out_gain:1;
		USR_INT16U padding:7;
	}bits;
}VCO_REG02_BIAS;

typedef union _VCO_REG03H_CFG{						// Write Only
	USR_INT16U	chip_config;						// Whole region
	// Wideband parts
	struct{											// Bits region
		USR_INT16U rf_buf_se_en:1;					// 1- single-ended mode(RF_N On);0- differential mode both RF_P and RF_N On
		USR_INT16U reserved0:1;						// Program 0
		USR_INT16U manu_rfo_mode:1;
		USR_INT16U rf_buf_bias:2;					// Program 10
		USR_INT16U spare:4;							// Don’t care
		USR_INT16U padding:7;
	}bits;
}VCO_REG03_CFG;
// Specified performance is only guaranteed with the required settings in this table.
// Other settings are not supported.
typedef union _VCO_REG04H_CAL_BIAS{					// Write Only
	USR_INT16U	chip_cal_bi;						// Whole region
	// Wideband parts
	struct{											// Bits region
		USR_INT16U vco_bias:3;						// Program 01
		USR_INT16U pll_buf_bias:2;					// Program 00
		USR_INT16U fl_bias:2;						// Program 10
		USR_INT16U preset_cal0:1;					// Program 01
		USR_INT16U padding:7;
	}bits;
}VCO_REG04_CAL_BIAS;		// required setting: 0x60A0

typedef union _VCO_REG05H_CF_CAL{					// Write Only
	USR_INT16U	chip_cf_cal;						// Whole region
	// Wideband parts
	struct{											// Bits region
		USR_INT16U cf_l:2;							// Program 00
		USR_INT16U cf_ml:2;							// Program 11
		USR_INT16U cf_mh:2;							// Program 10
		USR_INT16U cf_h:2;							// Program 00
		USR_INT16U spare:1;							// Program 0
		USR_INT16U padding:7;
	}bits;
}VCO_REG05_CF_CAL;			// required setting: 0x1628

typedef union _VCO_REG06H_MSB_CAL{					// Write Only
	USR_INT16U	chip_ms_cal;						// Whole region
	// Wideband parts
	struct{											// Bits region
		USR_INT16U msb_l:2;							// Program 11
		USR_INT16U msb_ml:2;						// Program 11
		USR_INT16U msb_mh:2;						// Program 11
		USR_INT16U msb_h:2;							// Program 11
		USR_INT16U spare:1;							// Don’t care
		USR_INT16U padding:7;
	}bits;
}VCO_REG06_MSB_CAL;			// required setting: 0x7FB0

typedef union OM_CMD_Struct{						// Open Mode SPI 32bits CMD&data structure
	USR_INT32U zero;
	union{
		struct{
			unsigned long addrbits:3;
			unsigned long regibits:5;
			unsigned long databits:24;
		}data_b;
		struct{
			unsigned long addrbits:3;
			unsigned long regibits:5;
			unsigned long addr_vco:3;
			unsigned long regi_vco:4;
			unsigned long data_vco:17;
		}data_v;
		struct{
			unsigned long addrbits:3;
			unsigned long regibits:5;
			USR_INT08U datas[3];
		}data_a;
	}body;
#define dat_b body.data_b
#define dat_v body.data_v
#define dat_a body.data_a
}OM_CMD_STRUCT;

/*
*********************************************************************************************************
*                                         PLL register address define
*********************************************************************************************************
*/

#define CHIP_ADDR		0x0
#define VCO_ID			0x0

#define REG00H			0x0
#define REG01H			0x01
#define REG02H			0x02
#define REG03H			0x03
#define REG04H			0x04
#define REG05H			0x05
#define REG06H			0x06
#define REG07H			0x07
#define REG08H			0x08
#define REG09H			0x09
#define REG0AH			0x0A
#define REG0BH			0x0B
#define REG0CH			0x0C
#define REG0DH			0x0D			// unused
#define REG0EH			0x0E			// unused
#define REG0FH			0x0F
#define REG10H			0x10
#define REG11H			0x11
#define REG12H			0x12
#define REG13H			0x13

#define VCO_REG00H		0x0
#define VCO_REG01H		0x01
#define VCO_REG02H		0x02
#define VCO_REG03H		0x03
#define VCO_REG04H		0x04
#define VCO_REG05H		0x05
#define VCO_REG06H		0x06

/*
*********************************************************************************************************
*                                         PLL register bits configuration define
*********************************************************************************************************
*/
//Reg 00h Open Mode Read Address/RST Strobe Register (Write Only)
#define SOFT_RST			1u
// Reg 01h RST Register
#define CEN_PIN				1u
#define CEN_SPI				2u
#define KEEP_BIAS_ON		1u
#define KEEP_PD_ON			1u
#define KEEP_CP_ON			1u
#define KEEP_REF_BUF_ON		1u
#define KEEP_VCO_ON			1u
#define KEEP_GPO_DRV_ON		1u
// Reg 02h REFDIV Register
// Reg 03h Frequency Register - Integer Part
// Reg 04h Frequency Register - Fractional Part
// Reg 05h VCO SPI Register
// Reg 06h SD CFG Register
#define SEED_0					0u			// 00: 0 seed
#define SEED_1					1u			// 01: lsb seed
#define SEED_2					2u			// 02: B29D08h seed
#define SEED_3					3u			// 03: 50F1CDh seed
#define ORDER_0					0u			// 0: 1st order
#define ORDER_1					1u			// 1: 2nd order
#define ORDER_2					2u			// 2: Type 1 fb
#define ORDER_3					3u			// 3: Type 2 ff
#define FRAC_BYPASS				0u			// 0: Use Modulator, Required for Fractional Mode,
											// 1: Bypass Modulator, Required for Integer Mode
#define AUTO_SEED				1u			// 1: loads the seed whenever the frac register is written
#define CLK_REF_SEL				1u			// 1: VCO divider clock (Recommended for normal operation)
#define SD_MODU_CLK_SEL			1u			// 1- SD VCO Clock delay (Recommended)
#define SD_EN					1u			// 1: Enable Frac Core, required for Fractional Mode, or Integer isolation testing
#define BIST_EN					1u			// Enable Built in Self Test
#define RD_BIST_C				0u			// RDiv BIST Cycles 00: 1032
#define AUTO_CLK_CFG			1u			// 
// Reg 07h Lock Detect Register
#define LD_WIN_MAX_5			0u			// 0: 5
#define LD_WIN_MAX_32			1u			// 1: 32
#define LD_WIN_MAX_96			2u			// 2: 96
#define LD_WIN_MAX_256			3u			// 3: 256
#define LD_WIN_MAX_512			4u			// 4: 512
#define LD_WIN_MAX_2048			5u			// 5: 2048
#define LD_WIN_MAX_8192			6u			// 6: 8192
#define LD_WIN_MAX_65535		7u			// 7: 65535
#define INTE_LD_EN				1u			// Enable Internal Lock Detect
#define LD_WIN_TYP_DIG			1u			// 1: Digital programmable timer
#define LD_WIN_TYP_ANL			0u			// 0: Analog one shot, nominal 10 ns window
#define LD_DIG_WIN_DUR_1_2		0u			// 0: 1/2 cycle
#define LD_DIG_WIN_DUR_1		1u			// 1: 1 cycle
#define LD_DIG_WIN_DUR_2		2u			// 2: 2 cycles
#define LD_DIG_WIN_DUR_4		3u			// 3: 4 cycles
#define LD_DIG_WIN_DUR_8		4u			// 4: 8 cycles
#define LD_DIG_WIN_DUR_16		5u			// 5: 16 cycles
#define LD_DIG_WIN_DUR_32		6u			// 6: 32 cycles
#define LD_DIG_WIN_DUR_64		7u			// 7: 64 cycles
#define DIG_TIM_FREQ_FST		0u			// Lock Detect Digital Timer Frequency Control 00: fastest
#define DIG_TIM_FREQ_SLW		3u			//  11: slowest
// Reg 08h Analog EN Register
#define BIAS_EN					1u			// 
#define CP_EN					1u			// 
#define PD_EN					1u			// 
#define REFBUF_EN				1u			// 
#define VCOBUF_EN				1u			// 
#define GPO_PAD_EN				1u			// 
#define VCO_DC2D_EN				1u			// VCO_Div_Clk_to_dig_en
#define PRESCAL_CLK_EN			1u			// 
#define VCOBUF_PREBIAS_EN		1u			// 
#define CP_INTE_OPA_EN			1u			// 
#define HIGH_FREQ_REF_EN		1u			// Program 1 for Ref In > =200 MHz, 0 <200MHz
// Reg 09h Charge Pump Register
#define CP_DN_GAIN(uA)			(uA / 20u)
#define CP_UP_GAIN(uA)			(uA / 20u)
#define OFFSET_MAGN(uA)			(uA / 5u)
#define OFFSET_UP_EN			1u			// Recommended setting = 0
#define OFFSET_DN_EN			1u			// Recommended setting = 1 in Fractional Mode, 0 otherwise
// Reg 0Ah VCO AutoCal Configuration Register
#define VT_RESOL_1				0u			// R Divider Cycles 0: - 1
#define VT_RESOL_2				1u			// 1 - 2
#define VT_RESOL_4				2u			// 2 - 4
#define VT_RESOL_8				3u			// 3 - 8
#define VT_RESOL_32				4u			// 4 - 32
#define VT_RESOL_64				5u			// 5 - 64
#define VT_RESOL_128			6u			// 6 - 128
#define VT_RESOL_256			7u			// 7 - 256
#define VCO_CURV_ADJ_DIS		0u			// Program 000 VCO Curve Adjustment vs Temp for AutoCal 0: - Disabled
#define VCO_CURV_ADJ_ADD1		1u			// 1 : + 1 Curve
#define VCO_CURV_ADJ_ADD2		2u			// 2: +2 Curves
#define VCO_CURV_ADJ_ADD3		3u			// 3: +3 Curves
#define VCO_CURV_ADJ_SUB4		4u			// 4: -4 Curves
#define VCO_CURV_ADJ_SUB3		5u			// 5: -3 Curves
#define VCO_CURV_ADJ_SUB2		6u			// 6: -2 Curves
#define VCO_CURV_ADJ_SUB1		7u			// 7: -1 Curve
#define WAIT_STATE_UP_NAR_TRI	0u			// Tmmt= 1 measurement cycle of AutoCal 0: Wait Only at Startup Use for Narrowband & Tri-band devices
#define WAIT_STATE_UP_WID_1T	1u			// 1: Wait on startup and after first Tmmtcycle Use for Wideband devices
#define WAIT_STATE_UP_2T		2u			// 2: Wait on startup and after first two Tmmtcycles
#define WAIT_STATE_UP_3T		3u			// 3: Wait on startup and after first three Tmmtcycles
#define NUM_SAR_VCO_8			0u			// Number of SAR bits in VCO 0: 8 - Use for Wideband devices
#define NUM_SAR_VCO_7			1u			// 1: 7 - Do not use
#define NUM_SAR_VCO_6			2u			// 2: 6 - Use for Narrowband & Tri-band devices
#define NUM_SAR_VCO_5			3u			// 3: 5 - Do not use
#define BYPASS_VCO_TUN			1u			// Program 0 for normal operation using VCO auto calibration
#define FSM_VSPI_CLK_SEL_1		0u			// Set the AutoCal FSM and VSPI Clock (50 MHz maximum) 0: Input Crystal Reference
#define FSM_VSPI_CLK_SEL_4		1u			// 1: Input Crystal Reference/4
#define FSM_VSPI_CLK_SEL_16		2u			// 2: Input Crystal Reference/16
#define FSM_VSPI_CLK_SEL_32		3u			// 3: Input Crystal Reference/32
// Reg 0Bh PD Register
#define PD_DEL_SEL				1u			// Program 001 Sets PD reset path delay
#define PD_UP_EN				1u			// Program 1 Enables the PD UP output
#define PD_DN_EN				1u			// Program 1 Enables the PD DN output
#define R_ID_HMC830				0x100u		// 
// Reg 0Fh GPO_SPI_RDIV Register
											// Signal selected here is output to SDO pin when enabled
#define GPO_SEL_REG0F_5			0u			// 0: Data from Reg0F[5]
#define GPO_SEL_LDOUT			1u			// 1: Lock Detect Output 
#define GPO_SEL_LDTRI			2u			// 2: Lock Detect Trigger
#define GPO_SEL_LDWINOUT		3u			// 3: Lock Detect Window Output
#define GPO_SEL_RING_OSC		4u			// 4: Ring Osc Test
#define GPO_SEL_PU_CSP			5u			// 5: Pullup Hard from CSP
#define GPO_SEL_PD_CSP			6u			// 6: PullDN hard from CSP
#define GPO_SEL_RESE			7u			// 7: Reserved
#define GPO_SEL_REF_BUF			8u			// 8: Reference Buffer Output
#define GPO_SEL_REF_DIV			9u			// 9: Ref Divider Output
#define GPO_SEL_VCO_DIV			10u			// 10: VCO divider Output 
#define GPO_SEL_MODU_CLK		11u			// 11: Modulator Clock from VCO divider
#define GPO_SEL_AUX_CLK			12u			// 12: Auxiliary Clock
#define GPO_SEL_AUX_SPI			13u			// 13: Aux SPI Clock
#define GPO_SEL_AUX_EN			14u			// 14: Aux SPI Enable
#define GPO_SEL_AUX_SPI_OUT		15u			// 15: Aux SPI Data Out
#define GPO_SEL_PD_DN			16u			// 16: PD DN
#define GPO_SEL_PD_UP			17u			// 17: PD UP
#define GPO_SEL_SD3_DLAY		18u			// 18: SD3 Clock Delay
#define GPO_SEL_SD3_CORE		19u			// 19: SD3 Core Clock
#define GPO_SEL_INTE_WR			20u			// 20: AutoStrobe Integer Write
#define GPO_SEL_FRAC_WR			21u			// 21: Autostrobe Frac Write
#define GPO_SEL_AUTO_AUX		22u			// 22: Autostrobe Aux SPI
#define GPO_SEL_SPI_L_EN		23u			// 23: SPI Latch Enable
#define GPO_SEL_VCO_D_S_R		24u			// 24: VCO Divider Sync Reset
#define GPO_SEL_SEED_L_S		25u			// 25: Seed Load Strobe
#define GPO_SEL_NOTUSED_26_29	0u			// 26-29: Not Used
#define GPO_SEL_SPI_BUF_EN		30u			// 30: SPI Output Buffer En
#define GPO_SEL_SOFT_RSTB		31u			// 31: Soft RSTB

/*
*********************************************************************************************************
*                                      VCO subsystem register bits configuration define
*********************************************************************************************************
*/
// VCO_Reg 00h Tuning
#define MST_EN_VCO_SUB			1u			// 
#define MAN_MO_PLL_BUF_EN		1u			// Enables PLL Buffer in manual mode only
#define MAN_MO_RF_BUF_EN		1u			// Enables RF Buffer to Output in manual mode only
#define MAN_MO_DIV_BY1_EN		1u			// Enables RF divide by 1 in manual mode only
#define MAN_MO_RF_DIV_EN		1u			// Enables RF divider in manual mode only
// VCO_Reg 02h Biases
#define RF_DIV_RADIO_MUTE		0u			// 0 - Mute, VCO and PLL buffer On, RF output stages Off
#define RF_DIV_RADIO_FO			1u			// 1 - Fo
#define RF_DIV_RADIO_FO_2		2u			// 2 - Fo/2
#define RF_DIV_RADIO_FO_3		3u			// 3 - invalid, defaults to 2
											// ...
#define RF_DIV_RADIO_FO_62		62u			// 62 - Fo/62
#define RF_OUT_BUF_GAIN_9DB		0u << 6		// 00 - Max Gain - 9 dB
#define RF_OUT_BUF_GAIN_6DB		1u << 6		// 01 - Max Gain - 6 dB
#define RF_OUT_BUF_GAIN_3DB		2u << 6		// 10 - Max Gain - 3 dB
#define RF_OUT_BUF_GAIN			3u << 6		// 11 - Max Gain
#define DIV_OUT_STAG_GAIN		1u << 8		// For VCO output Divide-by-1 or 2, set to 1 (0 reduces output power, degrading noise floor performance)
											// For VCO output Divide-by-4 or higher, set to 0. Use this bit to maintain flat output power across divider settings
// VCO_Reg 03h Config
#define RF_BUF_SE_EN			1u			// 
#define MAN_RFO_MO				1u			// 1 - ManualRFO mode, 0 - AutoRFO mode (recommended)
#define RF_BUF_BIAS				2u			// Program 10

#endif
