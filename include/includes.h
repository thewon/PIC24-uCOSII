/*
*********************************************************************************************************
*                                                uC/OS-II
*                                          The Real-Time Kernel
*
*                              (c) Copyright 1992-2004, Micrium, Weston, FL
*                                           All Rights Reserved
*
*                                           MASTER INCLUDE FILE
*********************************************************************************************************
*/

#ifndef     INCLUDES_H
#define     INCLUDES_H

//#include    <stdio.h>
//#include    <string.h>
//#include    <ctype.h>
//#include    <stdlib.h>

#include    <p24FJ256GB110.h>

#include    <cpu.h>
#include    <app_cfg.h>
#include	"app_task.h"
#include	"console.h"
#include    <ucos_ii.h>
#include    "bsp.h"

#endif

