#ifndef __MTYPE_H
#define __MTYPE_H

/*
*********************************************************************************************************
*                                   APPLICATION INDEPENDENT DATA TYPES
*
* Notes : These data types are chosen based upon the C30 compiler datatype specifications
*********************************************************************************************************
*/

typedef unsigned char			USR_BOOLEAN;
typedef const char				USR_CHAR;
typedef unsigned char			USR_INT08U;
typedef signed char				USR_INT08S;
typedef unsigned int			USR_INT16U;
typedef signed int				USR_INT16S;
typedef unsigned long			USR_INT32U;
typedef signed long				USR_INT32S;
typedef unsigned long long		USR_INT64U;
typedef signed long long		USR_INT64S;
typedef float					USR_FP32;
typedef double					USR_FP64;			// must enable C30 use 64-bit double

/*
*********************************************************************************************************
*                                             MACROS
*********************************************************************************************************
*/

#define  USR_TRUE				1
#define  USR_FALSE				0
#define  USR_NULL				(void*)0

#define  ENTRY_ADDR(func)		((USR_INT32U) __builtin_tblpage(func) << 16) + __builtin_tbloffset(func)

#endif
